# Changelog

All notable changes to this project will be documented in this file.
Each new release typically also includes the latest modulesync defaults.
These should not affect the functionality of the module.

## [v4.0.0](https://github.com/voxpupuli/puppet-augeasproviders_apache/tree/v4.0.0) (2022-07-06)

[Full Changelog](https://github.com/voxpupuli/puppet-augeasproviders_apache/compare/v3.2.0...v4.0.0)

**Breaking changes:**

- Drop EoL OS from metadata.json [\#14](https://github.com/voxpupuli/puppet-augeasproviders_apache/pull/14) ([bastelfreak](https://github.com/bastelfreak))

## [v3.2.0](https://github.com/voxpupuli/puppet-augeasproviders_apache/tree/v3.2.0) (2022-05-20)

[Full Changelog](https://github.com/voxpupuli/puppet-augeasproviders_apache/compare/3.1.1...v3.2.0)

**Closed issues:**

- Migrate from hercules-team to Vox Pupuli [\#12](https://github.com/voxpupuli/puppet-augeasproviders_apache/issues/12)
- Context with " [\#10](https://github.com/voxpupuli/puppet-augeasproviders_apache/issues/10)

## [3.1.1](https://github.com/voxpupuli/puppet-augeasproviders_apache/tree/3.1.1) (2019-03-01)

- Fix puppet requirement to < 7.0.0

## 3.1.0

- add support for Puppet 5 & 6
- deprecate support for Puppet < 5
- update supported OSes in metadata.json

## 3.0.0

- Fix support for 'puppet generate types'
- Remove misplaced stubs in spec tests
- Added CentOS and OracleLinux to supported OS list

## 2.0.2

- Upped supported Puppet versions to include Puppet 5

## 2.0.1

- Fix metadata.json
- Various minor updates to Travis test configuration

## 2.0.0

- First release of split module.


\* *This Changelog was automatically generated by [github_changelog_generator](https://github.com/github-changelog-generator/github-changelog-generator)*
